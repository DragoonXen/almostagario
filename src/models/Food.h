//
// Created by dragoon on 3/27/18.
//

#ifndef ALMOSTAGARIO_FOOD_H
#define ALMOSTAGARIO_FOOD_H


#include "GameObject.h"

class Food : public GameObject {
public:
    Food(double x, double y, int updateTick);
};


#endif //ALMOSTAGARIO_FOOD_H
