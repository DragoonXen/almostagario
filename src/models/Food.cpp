//
// Created by dragoon on 3/27/18.
//

#include "Food.h"
#include "../Constants.h"

Food::Food(double x, double y, int updateTick) : GameObject(0,
                                                          x,
                                                          y,
                                                          0.,
                                                          0.,
                                                          Constants::FOOD_MASS(),
                                                          Constants::FOOD_RADIUS,
                                                          updateTick) {}
