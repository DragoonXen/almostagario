//
// Created by dragoon on 4/8/18.
//

#ifndef ALMOSTAGARIO_SIMSTATE_H
#define ALMOSTAGARIO_SIMSTATE_H


#include <vector>
#include "simulation/SimCommand.h"
#include "../Constants.h"

class SimState {
public:
    std::vector<std::pair<SimCommand, int>> simCommands;
    double score;
    bool maxTtf;

    SimState(std::vector<std::pair<SimCommand, int>> &simCommands, std::pair<double, bool> score) : simCommands(std::vector<std::pair<SimCommand, int>>()),
                                                                                                    score(score.first),
                                                                                                    maxTtf(score.second) {
        this->simCommands.swap(simCommands);
    }

    void updateValues(std::vector<std::pair<SimCommand, int>> &simCommands, std::pair<double, bool> score) {
        if (score.first > this->score) {
            this->simCommands.swap(simCommands);
            this->score = score.first;
            this->maxTtf = score.second;
        }
        simCommands.clear();
    }

    void addBasicBonus() {
        this->score += 1e-6;//Constants::ADD_SCORE_FOR_PREV;
    }
};


#endif //ALMOSTAGARIO_SIMSTATE_H
