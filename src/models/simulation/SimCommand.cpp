//
// Created by dragoon on 4/2/18.
//

#include "SimCommand.h"

SimCommand::SimCommand(double x, double y) : x(x), y(y), split(false), eject(false) {}

SimCommand::SimCommand(double x, double y, bool split) : x(x), y(y), split(split), eject(false) {}

SimCommand::SimCommand(double x, double y, bool split, bool eject) : x(x), y(y), split(split), eject(eject) {}
