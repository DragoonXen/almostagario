//
// Created by dragoon on 4/1/18.
//

#ifndef ALMOSTAGARIO_SIMULATOR_H
#define ALMOSTAGARIO_SIMULATOR_H


#include "../../Context.h"
#include "SimCommand.h"
#include "../../ExtContext.h"

class Simulator {

private:
    static double simulate(Context context, const std::vector<std::pair<SimCommand, int>> &commandList, int totalTicks);

public:
    static std::pair<double, bool>
    simulateProxy(const ExtContext &context, const std::vector<std::pair<SimCommand, int>> &commandList, int totalTicks, Context const *const maxTtfContext);

#ifdef REWIND_VIEWER
public:
    static double debugSimulate(Context context, const std::vector<std::pair<SimCommand, int>> &commandList, int totalTicks);
#endif
};


#endif //ALMOSTAGARIO_SIMULATOR_H
