//
// Created by dragoon on 4/2/18.
//

#ifndef ALMOSTAGARIO_SIMCOMMAND_H
#define ALMOSTAGARIO_SIMCOMMAND_H


class SimCommand {
public:
    double x, y;
    bool split, eject;

    SimCommand(double x, double y);

    SimCommand(double x, double y, bool split);

    SimCommand(double x, double y, bool split, bool eject);
};


#endif //ALMOSTAGARIO_SIMCOMMAND_H
