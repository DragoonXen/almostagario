//
// Created by dragoon on 3/27/18.
//

#include "Constants.h"

std::unique_ptr<Constants> Constants::instance;

void Constants::initConstants(const json &json) {
    instance = std::make_unique<Constants>(Constants(json));
}

Constants::Constants(const json &json) {
    this->gameTicks = json["GAME_TICKS"];
    this->gameWidth = json["GAME_WIDTH"];
    this->gameHeight = json["GAME_HEIGHT"];
    this->foodMass = json["FOOD_MASS"];
    this->maxFragsCnt = json["MAX_FRAGS_CNT"];
    this->ticksTilFusion = json["TICKS_TIL_FUSION"];
    this->virusRadius = json["VIRUS_RADIUS"];
    this->virusSplitMass = json["VIRUS_SPLIT_MASS"];
    this->viscosity = json["VISCOSITY"];
    this->inertionFactor = json["INERTION_FACTOR"];
    this->speedFactor = json["SPEED_FACTOR"];
    this->halfWidth = this->gameWidth / 2;
    this->halfHeight = this->gameHeight / 2;
}

double Constants::EJECTION_RADIUS() {
    return instance->ejectionRadius;
}

double Constants::EJECTION_MASS() {
    return instance->ejectionMass;
}

double Constants::FOOD_MASS() {
    return instance->foodMass;
}

int Constants::GAME_WIDTH() {
    return instance->gameWidth;
}

int Constants::GAME_HEIGHT() {
    return instance->gameHeight;
}

int Constants::GAME_TICKS() {
    return instance->gameTicks;
}

double Constants::VIRUS_RADIUS() {
    return instance->virusRadius;
}

double Constants::VIRUS_SPLIT_MASS() {
    return instance->virusSplitMass;
}

double Constants::VISCOSITY() {
    return instance->viscosity;
}

double Constants::INERTION_FACTOR() {
    return instance->inertionFactor;
}

double Constants::SPEED_FACTOR() {
    return instance->speedFactor;
}

int Constants::MAX_FRAGS_CNT() {
    return instance->maxFragsCnt;
}

int Constants::TICKS_TIL_FUSION() {
    return instance->ticksTilFusion;
}

int Constants::HALF_WIDTH() {
    return instance->halfWidth;
}

int Constants::HALF_HEIGHT() {
    return instance->halfHeight;
}

void Constants::setMyId(int myId) {
    instance->myPlayerId = myId;
}

const double Constants::ENOUGHT_SEEN_DISTANCE = sqrt(HALF_COORDS_IN_ONE_FOOD_FIELD * HALF_COORDS_IN_ONE_FOOD_FIELD * 2) - Constants::FOOD_RADIUS;