//
// Created by dragoon on 3/27/18.
//
#ifdef REWIND_VIEWER

#include "../rewind/RewindClient.h"

#endif

#include "Solver.h"
#include "SimpleLog.h"
#include "Utils.h"
#include "InputParser.h"
#include "models/simulation/SimCommand.h"
#include "models/simulation/Simulator.h"
#include "models/SimState.h"

#define _USE_MATH_DEFINES

#include <math.h>

inline bool isNear(const ExtContext &context) {
    for (auto &player : context.me->parts) {
        if (Utils::dist(context.getPrevDestX(), context.getPrevDestY(), player.getX(), player.getY()) <= player.getRadius() * 1.2) {
            return true;
        }
    }
    return false;
}

inline bool meCanSplit(const ExtContext &context) {
    if (Constants::MAX_FRAGS_CNT() == context.me->parts.size()) {
        return false;
    }
    for (auto &playerPart : context.me->parts) {
        if (playerPart.getMass() > Constants::MIN_SPLIT_MASS) {
            return true;
        }
    }
    return false;
}

inline bool checkMinMaxTTF(const ExtContext &context) {
    for (auto &player : context.players) {
        if (&player == context.me) {
            continue;
        }
        if (player.parts.empty()) {
            continue;
        }
        if (player.parts.size() == 1 && player.parts.front().getId() == 0) { // solid players has exactly 0 ttf
            continue;
        }
        for (auto &part : player.parts) {
            auto pair = std::make_pair(part.getPlayerId(), part.getId());
            if (context.ttf.find(pair) == context.ttf.end()) { // no data for player, simulate both
                return true;
            }
        }
    }
    return false;
}

inline Context *maxContextTtf(const ExtContext &context) {
    auto *result = new Context(context);
    Context &contextCopy = *result;
    for (auto &player : contextCopy.players) {
        if (&player == contextCopy.me) { // no need in changes for me
            continue;
        }
        if (player.parts.empty()) {
            continue;
        }
        if (player.parts.size() == 1 && player.parts.front().getId() == 0) {
            continue;
        }
        for (auto &playerPart : player.parts) {
            auto pair = std::make_pair(playerPart.getPlayerId(), playerPart.getId());
            auto iter = context.ttf.lower_bound(pair);
            if (iter == context.ttf.end() || iter->first.first != pair.first) { // not found any greater id for this player - max ttf
                playerPart.updateTTF(Constants::TICKS_TIL_FUSION());
                continue;
            }
            if (iter->first.first == playerPart.getPlayerId() &&
                iter->first.second == playerPart.getId()) {// found exactly this player - no need in modifications
                continue;
            }
            playerPart.updateTTF(iter->second); // new ttf - ttf for next id for this player
        }
    }
    return result;
}

Command Solver::onTick(ExtContext &context, json &input) {
    InputParser::parse(context, input);

#ifdef REWIND_VIEWER
    {
        uint32_t playerColor[] = {0xFFFF00, 0xFF00FF, RewindClient::COLOR_BLUE, RewindClient::COLOR_GREEN, 0x00FFFF};
        RewindClient &client = RewindClient::instance();
        for (auto &player : context.me->parts) {
            client.circle(player.getX(), player.getY(), player.getRadius(), playerColor[context.me->id], 4);
            client.line(player.getX(), player.getY(), player.getX() + player.getDx() * 10, player.getY() + player.getDy() * 10, 0xFF0000, 4);

            double visShiftNorm = Constants::VIS_SHIFT / Utils::len(player.getDx(), player.getDy());
            double xVisionCenter = player.getX() + player.getDx() * visShiftNorm;
            double yVisionCenter = player.getY() + player.getDy() * visShiftNorm;

            double visionR;
            if (context.me->parts.size() == 1) {
                visionR = player.getRadius() * Constants::VIS_FACTOR;
            } else {
                visionR = player.getRadius() * Constants::VIS_FACTOR_FR * sqrt(context.me->parts.size());
            }
            client.circle(xVisionCenter, yVisionCenter, visionR, 0xaaFFFFFF, 2);
        }
        for (auto &player : context.players) {
//            LOG_DEBUG("%", player.getPlayerId());
            if (&player == context.me) {
                continue;
            }
            for (auto &playerPart : player.parts) {
                client.circle(playerPart.getX(), playerPart.getY(), playerPart.getRadius(), playerColor[player.id], 3);
                client.line(playerPart.getX(),
                            playerPart.getY(),
                            playerPart.getX() + playerPart.getDx() * 10,
                            playerPart.getY() + playerPart.getDy() * 10,
                            0xFF0000, 3);
            }
        }

        for (auto &food : context.viruses) {
            client.circle(food.getX(), food.getY(), food.getRadius(), 0, 3);
        }
        for (auto &food : context.ejections) {
            client.circle(food.getX(), food.getY(), food.getRadius(), 0xFF8800, 3);
        }
        for (auto &food : context.food) {
            client.circle(food.getX(), food.getY(), food.getRadius(), 0x00FF00, 3);
        }
    }
#endif
    auto &me = context.me;
    if (me->parts.empty()) {
        return Command(context.getPrevDestX(), context.getPrevDestY(), "dead");
    }
    auto &food = context.food;

    double midX = 0.;
    double midY = 0.;
    for (auto &part : me->parts) {
        midX += part.getX();
        midY += part.getY();
    }
    midX /= me->parts.size();
    midY /= me->parts.size();

    bool simulate = !food.empty();
    simulate |= !context.ejections.empty();
    if (!simulate) {
        for (auto &player : context.players) {
            if (&player == context.me) {
                continue;
            }
            if (!player.parts.empty()) {
                simulate = true;
            }
        }
    }
    simulate |= (!context.viruses.empty());
    if (simulate) {
        bool testSplit = meCanSplit(context);
        std::unique_ptr<Context> copy = checkMinMaxTTF(context) ? std::unique_ptr<Context>(maxContextTtf(context)) : nullptr;
        int simulateTicks = 50;
        double avgMaxSpeed = 0.;
        for (auto &playerPart : context.me->parts) {
            avgMaxSpeed += playerPart.getMaxSpeed();
        }
        avgMaxSpeed /= context.me->parts.size();
        int ticksforHalfMap = (int) ((Constants::GAME_WIDTH() * .25) / avgMaxSpeed);
        simulateTicks = std::min(ticksforHalfMap, simulateTicks);

        std::vector<std::pair<SimCommand, int>> commands;
        commands.emplace_back(SimCommand(context.getPrevDestX(), context.getPrevDestY()), simulateTicks);
        SimState basicSimState(commands, Simulator::simulateProxy(context, commands, simulateTicks, copy.get()));
        if (testSplit) {
            commands.emplace_back(SimCommand(context.getPrevDestX(), context.getPrevDestY(), true), 1);
            commands.emplace_back(SimCommand(context.getPrevDestX(), context.getPrevDestY()), simulateTicks - 1);
            basicSimState.updateValues(commands, Simulator::simulateProxy(context, commands, simulateTicks, copy.get()));
        }
        basicSimState.addBasicBonus();
        const int angleChange = 15;
        std::vector<SimState> scores;
//        scores.reserve(360 / angleChange * 2);
        for (int i = 0; i != 360; i += angleChange) { // every 20 degrees
            double angle = i * M_PI / 180.;
            double dx = cos(angle);
            double dy = sin(angle);
            Utils::toWall(midX, midY, dx, dy, 1.);
            double aimX = dx + midX;
            double aimY = dy + midY;
            aimX = std::min(std::max(0., aimX), (double) Constants::GAME_WIDTH());
            aimY = std::min(std::max(0., aimY), (double) Constants::GAME_HEIGHT());

            commands.emplace_back(SimCommand(aimX, aimY), simulateTicks);
            SimState simState(commands, Simulator::simulateProxy(context, commands, simulateTicks, copy.get()));
            if (testSplit) {
                commands.emplace_back(SimCommand(aimX, aimY, true), 1);
                commands.emplace_back(SimCommand(aimX, aimY), simulateTicks - 1);
                simState.updateValues(commands, Simulator::simulateProxy(context, commands, simulateTicks, copy.get()));
            }
            scores.push_back(simState);
        }
        bool applySimulation = basicSimState.simCommands.size() > 1;
        if (!applySimulation) {
            for (int i = 0; i != scores.size(); ++i) {
                if (scores[i].score != basicSimState.score) {
                    applySimulation = true;
                    break;
                }
            }
        }
        if (applySimulation) {
            double maxFound = scores[0].score;
            for (size_t i = 0; i != scores.size(); ++i) {
                maxFound = std::max(maxFound, scores[i].score);
            }
            if (maxFound <= basicSimState.score) {
#ifdef REWIND_VIEWER
                Simulator::debugSimulate(basicSimState.maxTtf ? *copy : context, basicSimState.simCommands, simulateTicks);
#endif
                return Command(context.getPrevDestX(), context.getPrevDestY(), basicSimState.simCommands[0].first.split, false);
            }
            size_t currMax = 0;
            size_t currStart = 0;
            size_t maxStart = 0;
            size_t maxLen = 0;
            for (size_t idx = 0; idx != (scores.size() * 2); ++idx) { // cycle check
                size_t i = idx;
                if (i > scores.size()) {
                    i -= scores.size();
                }
                if (scores[i].score == maxFound) {
                    ++currMax;
                } else {
                    if (currMax > maxLen) {
                        maxLen = currMax;
                        maxStart = currStart;
                    }
                    currStart = idx + 1;
                    currMax = 0;
                }
            }
            if (currMax > maxLen) {
                maxLen = currMax;
                maxStart = currStart;
            }
            size_t selected = maxStart + (maxLen + 1) / 2 - 1;
            if (selected > scores.size()) {
                selected -= scores.size();
            }

#ifdef REWIND_VIEWER
            Simulator::debugSimulate(scores[selected].maxTtf ? *copy : context, scores[selected].simCommands, simulateTicks);
#endif
            auto &bestDxDy = scores[selected].simCommands[0];
            return Command(bestDxDy.first.x, bestDxDy.first.y, bestDxDy.first.split, false);//3399
        }
    }

    if (!food.empty()) {
        double minDist = 1e70;
        Food *nearestFood = nullptr;
        double minRad = 1e10;
        for (auto &item : me->parts) {
            if (item.getRadius() < minRad) {
                minRad = item.getRadius();
            }
        }
        for (auto &it : food) {
            if (Utils::bannedFood(it, minRad)) {
                continue;
            }
            for (auto &item : me->parts) {
                double dist = it.distanceTo(item) - item.getRadius();
                if (dist < minDist) {
                    minDist = dist;
                    nearestFood = &it;
                }
            }
        }
        if (nearestFood != nullptr) {
            return Command(nearestFood->getX(), nearestFood->getY(), false, false);
        }
    }

    if (!context.dummy || isNear(context)) {
        double x = static_cast <double> (rand() * 1. / RAND_MAX) * Constants::GAME_WIDTH();
        double y = static_cast <double> (rand() * 1. / RAND_MAX) * Constants::GAME_HEIGHT();
        while (Utils::distSqr(x, y, midX, midY) < Constants::GAME_WIDTH() / 2.1) {
            x = static_cast <double> (rand() * 1. / RAND_MAX) * Constants::GAME_WIDTH();
            y = static_cast <double> (rand() * 1. / RAND_MAX) * Constants::GAME_HEIGHT();
        }
        context.dummy = true;
        return Command(x, y, "No food");
    } else {
        return Command(context.getPrevDestX(), context.getPrevDestY(), "No food");
    }
}